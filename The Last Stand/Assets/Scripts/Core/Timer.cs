using System;

public class Timer
{
    float seconds = 0;
    event Action onTimerEnd = null;
    public float RemainingSeconds { get; private set; }

    public Timer(float seconds = 0f, Action onTimerEnd = default)
    {
        this.seconds = seconds;
        this.onTimerEnd = onTimerEnd;
    }

    public void Tick(float deltaTime)
    {
        seconds -= deltaTime;
        RemainingSeconds = seconds;

        if (seconds < 0)
        {
            seconds = 0;
            onTimerEnd?.Invoke();
        }
    }
}
