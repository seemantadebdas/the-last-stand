using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Wraith : Ghost
{
    Animator animator = null;
    Intruder intruder = null;
    [SerializeField] float hauntTime = 5.0f;
    [SerializeField] float hauntRate = 1.0f;
    [SerializeField] UnityEvent onHaunt;

    Timer timer = null;
    public event Action OnDestroyEvent;

    List<Intruder> interactableObjects = new();

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();    
    }

    private void OnDestroy()
    {
        OnDestroyEvent?.Invoke();
        transform.DOKill();
    }

    public override bool CanPerformAction(RaycastHit hitInfo)
    {
        return hitInfo.collider.TryGetComponent(out Intruder _);
    }

    public override void PerformAction(RaycastHit hitInfo)
    {
        if (!hitInfo.collider.TryGetComponent(out Intruder intruder))
            return;
        
        this.intruder = intruder;
        Haunt();
    }

    protected override void Haunt()
    {
        onHaunt?.Invoke();  

        SetPositionAndRotationAroundIntruder();

        animator.Play("Blow");

        timer = new Timer(hauntTime, () => Destroy(gameObject));

        StartCoroutine(nameof(ContinuouslyHaunt));
    }

    private void SetPositionAndRotationAroundIntruder()
    {
        GameObject refObj = new GameObject("Ref Object");

        refObj.transform.SetParent(intruder.transform);
        refObj.transform.localPosition = Vector3.zero;
        transform.SetParent(refObj.transform);

        transform.position = refObj.transform.position;
        transform.position += refObj.transform.forward * 2.5f;
        transform.position += refObj.transform.up * 2f;
        transform.LookAt(intruder.transform.position);

        transform.DOLocalMoveY(1f, 1f).SetEase(Ease.InOutElastic).SetLoops(-1, LoopType.Yoyo);
        refObj.transform.DOLocalRotate(new(0, 360, 0), 2f, RotateMode.FastBeyond360).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Restart).SetRelative(true);
    }

    private void Update()
    {
        if (timer == null)
            return;

        timer.Tick(Time.deltaTime);
    }

    public override void InitializeGhost()
    {
        interactableObjects.Clear();
        Intruder[] allObjects = FindObjectsOfType<Intruder>();

        foreach(Intruder intruder in allObjects)
        {
            intruder.SetLayer(LayerMask.NameToLayer("Outline")); 
            interactableObjects.Add(intruder);
        }
    }

    public override void DisableGhost()
    {
        foreach(Intruder intruder in interactableObjects)
        {
            intruder.SetLayer(LayerMask.NameToLayer("Default"));
        }
    }

    IEnumerator ContinuouslyHaunt()
    {
        while (true)
        {
            intruder.Frighten(this, FearDealt);
            yield return new WaitForSeconds(hauntRate);
        }
    }
}
