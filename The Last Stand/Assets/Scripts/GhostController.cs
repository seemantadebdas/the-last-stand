using UnityEngine;
using UnityEngine.InputSystem;

public class GhostController : MonoBehaviour
{
    [SerializeField] Transform cursor = null;
    [SerializeField] float cursorSmoothTime = 0.1f;

    InputReader inputReader = null;
    GhostHandler ghostHandler = null;

    Transform currentGhostTransform = null;
    Ghost currentGhost = null;
    Vector3 cursorVelocity = default;
    Vector3 ghostVelocity = default;

    FearSystem fearSystem = null;
    
    private void Awake()
    {
        ghostHandler = GetComponent<GhostHandler>();        
        inputReader = GetComponent<InputReader>();
        fearSystem = GetComponent<FearSystem>();
    }

    private void OnEnable()
    {
        inputReader.OnActEvent += InputReader_OnActEvent;

        inputReader.OnCancelEvent += ResetCurrentGhost;

        ghostHandler.OnGhostChanged += GhostHandler_OnGhostChanged;

        Intruder.OnAnyIntruderFrightened += Intruder_OnAnyIntruderFrightened;
    }

    private void OnDisable()
    {
        inputReader.OnActEvent -= InputReader_OnActEvent;
        inputReader.OnCancelEvent -= ResetCurrentGhost;
        ghostHandler.OnGhostChanged -= GhostHandler_OnGhostChanged;
        Intruder.OnAnyIntruderFrightened -= Intruder_OnAnyIntruderFrightened;
    }

    private void ResetCurrentGhost()
    {
        Destroy(currentGhostTransform.gameObject);
        currentGhostTransform = null;
        currentGhost.DisableGhost();
        currentGhost = null;
        //cursor.GetComponent<MeshRenderer>().enabled = true;
    }

    void InputReader_OnActEvent()
    {
        if (currentGhost == null)
            return;

        Ray ray = GetRayFromMousePosition(); 
        if (Physics.Raycast(ray, out RaycastHit hit, 100f))
        {
            if (!currentGhost.CanPerformAction(hit))
                return;

            Ghost currentGhostPrefab = Instantiate(currentGhost);
            currentGhostPrefab.PerformAction(hit);
            fearSystem.DecreaseFearPoints(currentGhost.FearCost);

            ResetCurrentGhost();
        }
    }

    void GhostHandler_OnGhostChanged(Ghost newGhost, Ghost previousGhost)
    {
        if (currentGhostTransform != null)
        {
            ResetCurrentGhost();
        }
        currentGhostTransform = Instantiate(newGhost.GhostCursorPrefab, cursor.position, default).transform;
        currentGhost = newGhost;
        cursor.GetComponent<MeshRenderer>().enabled = false;
        currentGhost.InitializeGhost();
    }

    void Intruder_OnAnyIntruderFrightened(Intruder _, Ghost __, int fearDealt, Vector3 ___)
    {
        fearSystem.IncreaseFearPoints(fearDealt);
    }

    private void Update()
    {
        if(Physics.Raycast(GetRayFromMousePosition(), out RaycastHit hit ,100f))
        {
            cursor.position = Vector3.SmoothDamp(cursor.position, hit.point, ref cursorVelocity, cursorSmoothTime * Time.deltaTime);
            
            if(currentGhostTransform != null)
            { 
                currentGhostTransform.position = Vector3.SmoothDamp(currentGhostTransform.transform.position, hit.point, ref ghostVelocity, cursorSmoothTime * Time.deltaTime);
                currentGhost.CanPerformAction(hit); 
            }
        }
    }

    public Ray GetRayFromMousePosition()
    {
        return Camera.main.ScreenPointToRay(Mouse.current.position.value);
    }
}
