using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GameHandler : MonoBehaviour
{
    [SerializeField] UnityEvent onWin, onLoss, onGameStart;
    [SerializeField] float timeTillLoss = 120f;
    List<Intruder> totalIntruders = new List<Intruder>();

    Timer timer;
    InputReader inputReader;

    public float TimeRemaining
    {
        get => timer.RemainingSeconds;
    }

    public int IntrudersRemaining
    {
        get => totalIntruders.Count;
    }


    private void Awake()
    {
        inputReader = GetComponent<InputReader>();  

        totalIntruders = FindObjectsOfType<Intruder>().ToList();

        timer = new Timer(timeTillLoss, () =>
        {
            onLoss.Invoke();
        });
    }

    private void Start()
    {
        Time.timeScale = 0;
        Intruder.OnAnyIntruderDisabled += HandleIntruderRemoval;
    }

    private void OnEnable()
    {
        inputReader.OnCloseEvent += () =>
        {
            Time.timeScale = 1;
            onGameStart?.Invoke();
        };
    }


    private void OnDisable()
    {
        Intruder.OnAnyIntruderDisabled -= HandleIntruderRemoval;
    }

    private void Update()
    {
        if (timer.RemainingSeconds < 0)
            return;
        
        timer.Tick(Time.deltaTime);
    }

    private void HandleIntruderRemoval(Intruder intruder)
    {
        totalIntruders.Remove(intruder);

        if(totalIntruders.Count == 0)
        {
            onWin.Invoke();
        }
    }
}
