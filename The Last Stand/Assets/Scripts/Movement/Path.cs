using UnityEngine;

namespace TLS.Movement
{
    [System.Serializable]
    public struct WaypointData
    {
        public Vector3 position;
        public Vector3 rotation;
    }

    public class Path : MonoBehaviour
    {
        [field: SerializeField]
        public WaypointData[] WaypointDataArray { get; set; }
    }
}

