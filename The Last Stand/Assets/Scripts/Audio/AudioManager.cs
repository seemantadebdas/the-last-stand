using UnityEngine;

namespace Fable.Audio
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] AudioSource sfxAudioSource = null;

        public void PlaySFX(AudioDataSO audioData)
        {
            sfxAudioSource.PlayOneShot(audioData.GetRandomClip());
        }

        public void PlayAudioAtTransform(AudioDataSO audioData, Transform transform)
        {
            AudioSource.PlayClipAtPoint(audioData.GetRandomClip(), transform.position);
        }
    }
}