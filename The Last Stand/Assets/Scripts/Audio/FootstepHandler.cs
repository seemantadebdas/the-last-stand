using UnityEngine;

namespace Fable.Audio
{
    public enum EFootID
    {
        LEFT, RIGHT
    }

    [RequireComponent(typeof(AudioManager), typeof(Animator))]
    public class FootstepHandler : MonoBehaviour
    {
        [SerializeField] AudioDataSO audioData = null;

        AudioManager audioManager = null;
        Animator animator = null;
        private void Awake()
        {
            audioManager = GetComponent<AudioManager>();
            animator = GetComponent<Animator>();
        }

        public void PlayFootStepSound()
        {
            Transform footTransform;
            //if(foot == EFootID.LEFT)
            //{
            //}
            //else
            //{
            //    footTransform = animator.GetBoneTransform(HumanBodyBones.RightFoot);
            //}
            footTransform = animator.GetBoneTransform(HumanBodyBones.LeftFoot);

            audioManager.PlayAudioAtTransform(audioData, footTransform);
        }
    }
}