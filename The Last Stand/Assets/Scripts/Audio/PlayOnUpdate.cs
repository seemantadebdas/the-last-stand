using System.Collections.Generic;
using UnityEngine;

namespace Fable.Audio
{
    public class PlayOnUpdate : StateMachineBehaviour
    {
        [SerializeField] List<AudioDataPairUpdate> audioDataPairs;
        AudioManager audioManager = null;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (audioManager == null)
            {
                audioManager = animator.GetComponent<AudioManager>();
            }


            for(int i = 0;i < audioDataPairs.Count; i++)
            {
                audioDataPairs[i].hasTriggered = false;
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            for(int i = 0; i < audioDataPairs.Count; i++)
            {

                AudioDataSO audioData = audioDataPairs[i].audioData;
                HumanBodyBones audioTransform = audioDataPairs[i].audioTransform;
                float normalizedTimeThreshold = audioDataPairs[i].normalizedTimeThreshold;

                if (!audioDataPairs[i].hasTriggered && stateInfo.normalizedTime >= normalizedTimeThreshold)
                {
                    audioDataPairs[i].hasTriggered = true;

                    if (audioTransform == HumanBodyBones.LastBone)
                    {
                        audioManager.PlaySFX(audioData);
                        continue;
                    }

                    audioManager.PlayAudioAtTransform(audioData, animator.GetBoneTransform(audioTransform));
                }
            }
        }
    }
}