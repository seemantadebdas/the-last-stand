using System.Collections.Generic;
using UnityEngine;

namespace Fable.Audio
{
    [CreateAssetMenu(fileName = "New AudioData", menuName = "Audio/AudioData")]
    public class AudioDataSO : ScriptableObject
    {
        [SerializeField] List<AudioClip> audioClipList = null;

        public AudioClip GetRandomClip()
        {
            return audioClipList[Random.Range(0, audioClipList.Count)];
        }
    }

    [System.Serializable]
    public class AudioDataPair
    {
        public AudioDataSO audioData;
        public HumanBodyBones audioTransform;
    }

    [System.Serializable]
    public class AudioDataPairUpdate : AudioDataPair
    {
        public float normalizedTimeThreshold;
        [HideInInspector] public bool hasTriggered;
    }
}
    