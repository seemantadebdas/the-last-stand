using System.Collections.Generic;
using UnityEngine;

namespace Fable.Audio
{
    public class PlayOnExit : StateMachineBehaviour
    {
        [SerializeField] List<AudioDataPair> audioDataPairs;
        AudioManager audioManager = null;

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (audioManager == null)
            {
                audioManager = animator.GetComponent<AudioManager>();
            }

            for (int i = 0; i < audioDataPairs.Count; i++)
            {

                AudioDataSO audioData = audioDataPairs[i].audioData;
                HumanBodyBones audioTransform = audioDataPairs[i].audioTransform;

                if (audioTransform == HumanBodyBones.LastBone)
                {
                    audioManager.PlaySFX(audioData);
                    continue;
                }

                audioManager.PlayAudioAtTransform(audioData, animator.GetBoneTransform(audioTransform));
            }
        }
    }
}