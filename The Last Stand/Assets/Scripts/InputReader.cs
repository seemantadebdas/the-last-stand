using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReader : MonoBehaviour, Controls.IPlayerActions
{
    Controls controls = null;
    public Vector2 MoveInput { get; private set; }
    public float RotateInput {  get; private set; }
    public event Action OnActEvent;
    public event Action OnNum1Event, OnNum2Event, OnNum3Event;
    public event Action OnCancelEvent;
    public event Action OnCloseEvent;

    private void OnEnable()
    {
        if (controls == null)
        {
            controls = new Controls();
            controls.Player.SetCallbacks(this);
        }
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        MoveInput = context.ReadValue<Vector2>();
    }

    public void OnRotate(InputAction.CallbackContext context)
    {
        RotateInput = context.ReadValue<float>();   
    }

    public void OnAct(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;

        OnActEvent?.Invoke(); 
    }

    public void OnNum1(InputAction.CallbackContext context)
    {
        if(context.performed) OnNum1Event?.Invoke();
    }

    public void OnNum2(InputAction.CallbackContext context)
    {
        if (context.performed) OnNum2Event?.Invoke();
    }

    public void OnNum3(InputAction.CallbackContext context)
    {
        if (context.performed) OnNum3Event?.Invoke();
    }

    public void OnCancel(InputAction.CallbackContext context)
    {
        if(context.performed) OnCancelEvent?.Invoke();
    }

    public void OnClose(InputAction.CallbackContext context)
    {
        if (context.performed) OnCloseEvent?.Invoke();
    }
}
