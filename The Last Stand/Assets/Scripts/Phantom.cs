using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class Phantom : Ghost
{
    [SerializeField] LayerMask groundLayer;
    [SerializeField] UnityEvent onFrighten;
    Animator animator = null;
    Sequence idleSequence = null;
    List<GameObject> interactableObjects = new();

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
    }

    public override bool CanPerformAction(RaycastHit hitInfo)
    {
        int hitLayer = hitInfo.transform.gameObject.layer;
        if (((1<< hitLayer) & groundLayer) != 0)
        {
            transform.position = hitInfo.point;
            return true;
        }

        return false;
    }

    public override void PerformAction(RaycastHit hitInfo)
    {
        int hitLayer = hitInfo.transform.gameObject.layer;
        if (((1 << hitLayer) & groundLayer) != 0)
        {
            transform.position = hitInfo.point;
            Haunt();
        }
    }

    protected override void Haunt()
    {
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.y = 0;

        transform.LookAt(cameraPos);
        transform.position -= transform.up * 0.75f;

        idleSequence = DOTween.Sequence();
        idleSequence.AppendInterval(1.5f);
        idleSequence.Append(transform.DOLocalMoveY(-0.3f, 0.25f).SetEase(Ease.InOutBounce));
        idleSequence.Append(transform.DOLocalRotate(new Vector3(0, -30, 0), 1f, RotateMode.LocalAxisAdd).SetEase(Ease.OutBounce));
        idleSequence.AppendInterval(1f);
        idleSequence.Append(transform.DOLocalRotate(new Vector3(0, 60, 0), 1f, RotateMode.LocalAxisAdd).SetEase(Ease.OutBounce));
        idleSequence.AppendInterval(1f);
        idleSequence.Append(transform.DOLocalRotate(new Vector3(0, -30, 0), 1f, RotateMode.LocalAxisAdd).SetEase(Ease.OutBounce));
        idleSequence.Append(transform.DOLocalMoveY(-0.75f, 0.25f).SetEase(Ease.InOutBounce));
        idleSequence.AppendInterval(1.5f);
        idleSequence.SetLoops(-1, LoopType.Yoyo);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out Intruder intruder))
        {
            transform.DOKill();
            idleSequence.Kill();

            transform.LookAt(intruder.transform.position);
            transform.position = new(transform.position.x, 0, transform.position.z); 
            animator.Play("Jump");

            transform.DOLocalMoveY(1f, 0.33f).SetEase(Ease.OutBounce);

            var sequence = DOTween.Sequence(); 
            sequence.Append(transform.DOScale(Vector3.one * 1.5f, 0.25f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                intruder.Frighten(this, FearDealt);
                onFrighten?.Invoke();
            }));

            sequence.Append(transform.DOShakeRotation(1f, Vector3.forward * 45));

            sequence.Append(transform.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                transform.DOKill();
                sequence.Kill();
                Debug.Log("Complete");
                Destroy(gameObject);
            }));
        }
    }

    public override void InitializeGhost()
    {
        interactableObjects.Clear(); 
        GameObject[] allObjects = FindObjectsOfType<GameObject>();
        
        for(int i = 0; i < allObjects.Length; i++)
        {
            GameObject obj = allObjects[i];

            if (((1 << obj.layer) & groundLayer) != 0)
            {
                interactableObjects.Add(obj);
                obj.layer = LayerMask.NameToLayer("Outline");
            }
        }
    }

    public override void DisableGhost()
    {
        foreach(GameObject obj in interactableObjects)
        {
            obj.layer = LayerMask.NameToLayer("Ground");
        }
    }
}
