using UnityEngine;

[CreateAssetMenu(fileName = "New GhostData", menuName = "ScriptableObjects/GhostData", order = 1)]
public class GhostDataSO : ScriptableObject
{
    [field: SerializeField] public Ghost GhostPrefab { get; private set; }
    [field: SerializeField] public GameObject GhostCursorPrefab { get; private set; }

}
