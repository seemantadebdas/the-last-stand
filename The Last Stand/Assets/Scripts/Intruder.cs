using System;
using System.Collections.Generic;
using UnityEngine;

public class Intruder : MonoBehaviour
{
    public event Action<Ghost,int, Vector3> OnFrightened;
    public static event Action<Intruder, Ghost, int, Vector3> OnAnyIntruderFrightened;
    public event Action OnIntruderFleeInitiated;
    public static event Action<Intruder> OnAnyIntruderDisabled;

    bool completelyFrightened = false;
    
    [SerializeField] int maxFearLimit = 50;
    int currentFearLimit = 0;

    [SerializeField] List<GameObject> visualsList = null;

    private void Awake()
    {
        currentFearLimit = maxFearLimit;    
    }

    private void OnDisable()
    {
        OnAnyIntruderDisabled?.Invoke(this);
    }

    public void Frighten(Ghost ghost,int fearDealt, Vector3 position = default)
    {
        if(currentFearLimit - fearDealt < 0 && !completelyFrightened)
        {
            completelyFrightened = true;
            OnIntruderFleeInitiated?.Invoke();
            return;
        }

        currentFearLimit -= fearDealt;

        if(position == default)
        {
            OnFrightened?.Invoke(ghost,fearDealt, ghost.transform.position);
        }
        OnFrightened?.Invoke(ghost,fearDealt, position);
        OnAnyIntruderFrightened?.Invoke(this, ghost, fearDealt, position);
    }

    public void SetLayer(string layer)
    {
        foreach(GameObject go in visualsList) 
        {
            go.layer = LayerMask.NameToLayer(layer);
        }
    }

    public void SetLayer(int layer)
    {
        foreach (GameObject go in visualsList)
        {
            go.layer = layer;
        }
    }
}
