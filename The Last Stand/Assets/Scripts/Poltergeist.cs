using System.Collections.Generic;
using UnityEngine;

public class Poltergeist : Ghost
{
    [SerializeField] float possessTime = 5f;

    List<PossessableObject> interactableObjects = new();
    public override bool CanPerformAction(RaycastHit hitInfo)
    {
        return hitInfo.collider.TryGetComponent(out PossessableObject _);
    }

    public override void InitializeGhost()
    {
        interactableObjects.Clear();
        PossessableObject[] allObjects = FindObjectsOfType<PossessableObject>();

        foreach (PossessableObject obj in allObjects)
        {
            obj.SetLayer(LayerMask.NameToLayer("Outline"));
            interactableObjects.Add(obj);
        }
    }

    public override void DisableGhost()
    {
        foreach (PossessableObject obj in interactableObjects)
        {
            obj.SetLayer(LayerMask.NameToLayer("Default"));
        }
    }


    public override void PerformAction(RaycastHit hitInfo)
    {
        if (!hitInfo.collider.TryGetComponent(out PossessableObject possessable))
            return;

        possessable.Posseess(this, possessTime);
        Haunt();
    }

    protected override void Haunt()
    {
        Destroy(gameObject);
    }
}
