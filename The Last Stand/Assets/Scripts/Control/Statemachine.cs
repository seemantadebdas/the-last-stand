using UnityEngine;

public class Statemachine : MonoBehaviour
{
    protected State currentState = null;

    public virtual void SwitchState(State newState)
    {
        currentState?.Exit();
        currentState = newState;
        currentState?.Enter();
    }

    private void Update()
    {
        currentState?.Tick(Time.deltaTime);
    }
}
