using DG.Tweening;
using UnityEngine;

public class IntruderScaredWalkState : IntruderExploreState
{

    Ghost ghost = null;
    public IntruderScaredWalkState(IntruderStatemachine sm, Ghost ghost) : base(sm)
    {
        this.ghost = ghost;
    }

    public override void Enter()
    {
        //override top layer animation with scared animation
        //base.Enter();
        if (sm.Path == null)
        {
            Debug.LogWarning("Path not set. Switching to Idle state");
            sm.SwitchState(new IntruderIdleState(sm));
            return;
        }

        waypointDataArray = sm.Path.WaypointDataArray;
        currentWaypointIndex = sm.NextWaypointIndex;
        sm.Agent.isStopped = false;

        sm.Animator.CrossFadeInFixedTime("Walking", 0.1f);
        sm.Animator.CrossFadeInFixedTime("Terrified", 0.1f, 1);

        DOVirtual.Float(0, 1, 0.1f, v => sm.Animator.SetLayerWeight(1, v));

        if(ghost is Wraith wraith)
        {
            wraith.OnDestroyEvent += () =>
            {
                sm.SwitchState(new IntruderExploreState(sm));
            };
        }
    }

    public override void Exit()
    {
        //reset animator
        sm.Animator.CrossFadeInFixedTime("EMPTY", 0.1f, 1);
        DOVirtual.Float(1, 0, 0.1f, v => sm.Animator.SetLayerWeight(1, v));
    }
}
