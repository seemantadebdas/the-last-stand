using TLS.Movement;
using UnityEngine;
using UnityEngine.AI;

public class IntruderStatemachine : Statemachine
{
    [field: SerializeField] public Path Path { get; private set; } = null;
    [field: SerializeField] public Transform FleePosition { get; private set; } = null; 
    public NavMeshAgent Agent { get; private set; } = null;
    public Animator Animator { get; private set; } = null;
    public int NextWaypointIndex { get; set; } = 0;

    public Intruder Intruder { get; private set; } = null;
    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        Intruder = GetComponent<Intruder>();
        Animator = GetComponent<Animator>();

        Vector3 sourcePostion = transform.position;//The position you want to place your agent
        if (NavMesh.SamplePosition(sourcePostion, out NavMeshHit closestHit, 500, 1))
        {
            transform.position = closestHit.position;
            //TODO
        }
    }

    private void Start()
    {
        SwitchState(new IntruderExploreState(this));
    }

    private void OnEnable()
    {
        Intruder.OnIntruderFleeInitiated += SwitchToFleeState;
    }

    private void OnDisable()
    {
        Intruder.OnIntruderFleeInitiated -= SwitchToFleeState;
    }

    public override void SwitchState(State state)
    {
        IntruderBaseState previousState = currentState as IntruderBaseState;
        IntruderBaseState nextState = state as IntruderBaseState;

        HandleStateExit(previousState);

        base.SwitchState(state);
    }

    void SwitchToFleeState()
    {
        SwitchState(new IntruderFleeState(this));
        return;
    }

    void HandleStateExit(IntruderBaseState previousState)
    {
        switch(previousState)
        {
            case IntruderFleeState:
                {
                    Destroy(gameObject);
                }
                break;
        }
    }
}
