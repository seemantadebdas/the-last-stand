using TLS.Movement;
using UnityEngine;
using UnityEngine.AI;

public class IntruderExploreState : IntruderBaseState
{
    protected int currentWaypointIndex = 0;
    protected WaypointData[] waypointDataArray = null;

    public IntruderExploreState(IntruderStatemachine sm) : base(sm)
    {
    }

    public override void Enter()
    {
        if(sm.Path == null)
        {
            Debug.LogWarning("Path not set. Switching to Idle state");
            sm.SwitchState(new IntruderIdleState(sm));
            return;
        }
        
        waypointDataArray = sm.Path.WaypointDataArray;
        currentWaypointIndex = sm.NextWaypointIndex;
        sm.Agent.isStopped = false;

        sm.Animator.CrossFadeInFixedTime("Walking", 0.1f);

        sm.Intruder.OnFrightened += SwitchToScaredState;
    }

    public override void Exit()
    {
        sm.Intruder.OnFrightened -= SwitchToScaredState;
    }

    public override void Tick(float deltaTime)
    {
         MoveToWaypoint();
    }

    private void MoveToWaypoint()
    {
        Vector3 waypointPosition = waypointDataArray[currentWaypointIndex].position;//The position you want to place your agent
        if (NavMesh.SamplePosition(waypointPosition, out NavMeshHit closestHit, 500, 1))
        {
            waypointPosition = closestHit.position;
            //TODO
        }

        float distanceToWaypoint = Vector3.Distance(sm.transform.position, waypointPosition);

        if (distanceToWaypoint <= sm.Agent.stoppingDistance)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % waypointDataArray.Length;
            sm.NextWaypointIndex = currentWaypointIndex;

            Debug.Log("Switching to Idle");
            sm.SwitchState(new IntruderIdleState(sm));
            return;
        }

        sm.Agent.SetDestination(waypointDataArray[currentWaypointIndex].position);
    }
    void SwitchToScaredState(Ghost ghost, int fearDealt, Vector3 position)
    {
        Debug.Log("Idle Switching Scared state");
        switch (ghost)
        {
            case Phantom phantom:
                {
                    sm.SwitchState(new IntruderScaredRunAwayState(sm, phantom));
                    break;
                }
            case Wraith wraith:
                {
                    sm.SwitchState(new IntruderScaredWalkState(sm, wraith));
                    break;
                }
            case Poltergeist poltergeist:
                {
                    sm.SwitchState(new IntruderScaredRunAwayState(sm, position));
                    break;
                }
        }
    }

}
