using UnityEngine;
using UnityEngine.AI;

public class IntruderFleeState : IntruderBaseState
{
    Vector3 destination = Vector3.zero;
    public IntruderFleeState(IntruderStatemachine sm) : base(sm)
    {
    }

    public override void Enter()
    {
        destination = sm.FleePosition.position;
        if (NavMesh.SamplePosition(destination, out NavMeshHit closestHit, 500, 1))
        {
            destination = closestHit.position;
        }

        sm.Agent.SetDestination(destination);
        sm.Animator.CrossFadeInFixedTime("Scared Run", 0.1f);
    }

    public override void Exit()
    {
        
    }

    public override void Tick(float deltaTime)
    {
        float distanceToWaypoint = Vector3.Distance(sm.transform.position, destination);
        Debug.Log(distanceToWaypoint);
        if (distanceToWaypoint <= sm.Agent.stoppingDistance)
        {
            sm.SwitchState(new IntruderIdleState(sm));
            return;
        }
    }
}
