using UnityEngine;

public class IntruderIdleState : IntruderBaseState
{
    readonly float waypointWaitTime = 5f;

    Timer timer;
    public IntruderIdleState(IntruderStatemachine sm) : base(sm)
    {
    }

    public override void Enter()
    {
        sm.Animator.CrossFadeInFixedTime("Idle", 0.1f);

        timer = new Timer(waypointWaitTime, () =>
        {
            sm.SwitchState(new IntruderExploreState(sm)); 
        });

        sm.Intruder.OnFrightened += SwitchToScaredState;
    }

    public override void Exit()
    {
        sm.Intruder.OnFrightened -= SwitchToScaredState;
    }

    public override void Tick(float deltaTime)
    {
        timer.Tick(deltaTime);
    }

    void SwitchToScaredState(Ghost ghost, int fearDealt, Vector3 position)
    {
        Debug.Log("Idle Switching Scared state");
        switch (ghost)
        {
            case Phantom phantom:
                {
                    sm.SwitchState(new IntruderScaredRunAwayState(sm, phantom));
                    break;
                }
            case Wraith wraith:
                {
                    sm.SwitchState(new IntruderScaredWalkState(sm, wraith));
                    break;
                }
            case Poltergeist poltergeist:
                {
                    sm.SwitchState(new IntruderScaredRunAwayState(sm, position));
                    break;
                }
        }
    }
}
