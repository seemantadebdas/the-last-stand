public abstract class IntruderBaseState : State
{
    protected IntruderStatemachine sm = null;

    public IntruderBaseState(IntruderStatemachine sm)
    {
        this.sm = sm;
    }
}
