using UnityEngine;

public class IntruderScaredRunAwayState : IntruderBaseState
{
    enum InternalState
    {
        FLEEING,
        SITTING
    }

    InternalState state;
    Vector3 instigatorPosition;
    Timer timer;

    public IntruderScaredRunAwayState(IntruderStatemachine sm, Ghost instigator) : base(sm)
    {
        instigatorPosition = instigator.transform.position;
    }

    public IntruderScaredRunAwayState(IntruderStatemachine sm, Vector3 position) : base(sm)
    {
        instigatorPosition = position;
    }

    public override void Enter()
    {
        sm.Animator.CrossFadeInFixedTime("Scared Run", 0.1f);

        timer = new Timer(5f, () =>
        {
            state = InternalState.SITTING;
            sm.Agent.isStopped = true;
            sm.Animator.CrossFadeInFixedTime("Terrified", 0.1f);

            timer = new Timer(5f, () =>
            {
                sm.SwitchState(new IntruderExploreState(sm));
            });
        });
    }

    public override void Exit()
    {
        
    }

    public override void Tick(float deltaTime)
    {
        Vector3 direction = sm.transform.position - instigatorPosition;
        direction.Normalize();

        switch (state)
        {
            case InternalState.FLEEING:
                sm.Agent.SetDestination(sm.transform.position + direction);
                break;
            case InternalState.SITTING:
                break;
        }

        timer.Tick(deltaTime);
    }
}
