using System;
using UnityEngine;

public class GhostHandler : MonoBehaviour
{
    [SerializeField] Ghost phantomGhost = null;
    [SerializeField] Ghost wraithGhost = null;
    [SerializeField] Ghost poltergiestGhost = null;
    
    InputReader inputReader = null;
    Ghost currentGhost = null;
    FearSystem fearSystem = null;
    /// <summary>
    /// param1: New Ghost
    /// param2: Previous Ghost
    /// </summary>
    public event Action<Ghost, Ghost> OnGhostChanged;
    private void Awake()
    {
        inputReader = GetComponent<InputReader>();
        fearSystem  = GetComponent<FearSystem>();
    }

    private void OnEnable()
    {
        //Change the ghost changning resposibility to some other script
        inputReader.OnNum1Event += () => ChangeGhost(phantomGhost);
        inputReader.OnNum2Event += () => ChangeGhost(wraithGhost);
        inputReader.OnNum3Event += () => ChangeGhost(poltergiestGhost);

        inputReader.OnCancelEvent += ResetCurrentGhost;
    }

    private void OnDisable()
    {
        inputReader.OnCancelEvent -= ResetCurrentGhost;
    }

    void ChangeGhost(Ghost ghost)
    {
        if (ghost.FearCost > fearSystem.CurrentFearPoints)
            return;

        if (currentGhost != null && currentGhost == ghost)
            return;

        OnGhostChanged?.Invoke(ghost, currentGhost);
    }
    private void ResetCurrentGhost()
    {
        currentGhost = null;
    }
}
