using UnityEngine;

public class FearSystem : MonoBehaviour
{
    [SerializeField] int initialFearPoints = 50;
    [field: SerializeField] public int CurrentFearPoints { get; private set; } = 0;

    private void Awake()
    {
        CurrentFearPoints = initialFearPoints;
    }

    public void IncreaseFearPoints(int fearPoints)
    {
        CurrentFearPoints += fearPoints;
    }

    public void DecreaseFearPoints(int fearPoints)
    {
        CurrentFearPoints -= fearPoints;
    }
}
