using Fable.Audio;
using UnityEngine;

public class BackgroundMusicController : MonoBehaviour
{
    AudioSource audioSource = null;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudio(AudioDataSO audioData)
    {
        audioSource.clip = audioData.GetRandomClip();
        audioSource.Play();
    }
}
