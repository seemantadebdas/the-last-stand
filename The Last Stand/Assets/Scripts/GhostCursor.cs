using DG.Tweening;
using UnityEngine;

public class GhostCursor : MonoBehaviour
{
    private void Start()
    {
        GetComponentInChildren<Animator>().CrossFadeInFixedTime("Dance", 0.1f);

        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.33f).SetEase(Ease.InOutElastic);
    }

    void Update()
    {
        Vector3 cameraPos = Camera.main.transform.position;
        cameraPos.y = 0;

        transform.LookAt(cameraPos);
    }
}
