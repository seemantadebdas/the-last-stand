using TMPro;
using UnityEngine;

public class TimerUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timeText;
    GameHandler gameHandler;

    private void Start()
    {
        gameHandler = FindObjectOfType<GameHandler>();
    }

    private void Update()
    {
        int timeRemaining = (int)gameHandler.TimeRemaining;
        
        timeText.text = timeRemaining.ToString();
    }
}
