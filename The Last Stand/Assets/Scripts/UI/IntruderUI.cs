using TMPro;
using UnityEngine;

public class IntruderUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI intruderText;
    GameHandler gameHandler;

    private void Start()
    {
        gameHandler = FindObjectOfType<GameHandler>();
    }

    private void Update()
    {
        intruderText.text = gameHandler.IntrudersRemaining.ToString();
    }
}
