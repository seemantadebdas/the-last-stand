using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float moveSpeed = 1.0f;
    [SerializeField] float rotateSpeed = 50f;
    [SerializeField] InputReader inputReader = null;

    private void LateUpdate()
    {
        Vector3 input = new Vector3(inputReader.MoveInput.x, 0, inputReader.MoveInput.y).normalized;

        Vector3 cameraRelativeInput = Camera.main.transform.TransformDirection(input);
        Vector3 projectedVector = Vector3.ProjectOnPlane(cameraRelativeInput, Vector3.up);

        transform.Translate(projectedVector * moveSpeed * Time.deltaTime, Space.World);

        transform.eulerAngles -= inputReader.RotateInput * rotateSpeed * Time.deltaTime * Vector3.up; 
    }
}
