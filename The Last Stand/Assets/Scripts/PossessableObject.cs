using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PossessableObject : MonoBehaviour
{
    [SerializeField] float influenceRadius = 1.0f;
    [SerializeField] List<GameObject> visualsList = null;
    [SerializeField] Color gizmoColor = new(255, 0, 0, 0.5f);
    [SerializeField] UnityEvent onFrighten;

    Collider[] overlapResultArray = new Collider[5];
    Poltergeist poltergeist = null;
    Sequence possessSeq = null;
    Timer timer = null;

    List<Intruder> intruderList = new();

    public void Posseess(Poltergeist poltergeist, float possessTime)
    {
        this.poltergeist = poltergeist;
        Debug.Log("Possessed: " + poltergeist.transform.name + "This: "+ this.poltergeist + " " + possessTime);

        timer = new Timer(possessTime, () =>
        {
            Debug.Log("Timer over");


            intruderList.Clear();
            this.poltergeist = null;
            possessSeq.Kill();
            timer = null;
        });
    }

    private void Update()
    {
        if (timer == null)
            return;

        timer.Tick(Time.deltaTime);

        Array.Clear(overlapResultArray, 0, overlapResultArray.Length);
        int colliderCount = Physics.OverlapSphereNonAlloc(transform.position, influenceRadius, overlapResultArray);

        if (colliderCount > 0)
        {
            foreach(Collider collider in overlapResultArray)
            {
                if(collider == null)
                {
                    continue;
                }

                if (!collider.TryGetComponent(out Intruder intruder))
                    continue;

                if (!IsInfront(intruder))
                    continue;

                if(intruderList.Contains(intruder)) 
                    continue;

                intruderList.Add(intruder);

                Debug.Log("Frightened Intruder");
                intruder.Frighten(poltergeist, poltergeist.FearDealt ,transform.position);
                onFrighten?.Invoke();
                SetAnimation();
            }
        }
    }

    bool IsInfront(Intruder intruder)
    {
        var heading = intruder.transform.position - transform.position;
        float dot = Vector3.Dot(heading, transform.forward);

        return (dot > 0.5);
    }

    private void SetAnimation()
    {
        possessSeq = DOTween.Sequence();

        possessSeq.Append(transform.DOJump(transform.position, 2, 0, 1).SetEase(Ease.Linear));
        possessSeq.Join(transform.DOShakeRotation(0.5f, new Vector3(0, 0, 30)).SetEase(Ease.Flash));
        possessSeq.AppendInterval(1f);
        //possessSeq.SetLoops(-1, LoopType.Restart);
    }
    public void SetLayer(string layer)
    {
        foreach (GameObject go in visualsList)
        {
            go.layer = LayerMask.NameToLayer(layer);
        }
    }

    public void SetLayer(int layer)
    {
        foreach (GameObject go in visualsList)
        {
            go.layer = layer;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Something entered " + poltergeist);
        
        //if (poltergeist == null)
        //    return;
        
        //if(other.TryGetComponent(out Intruder intruder))
        //{
        //    Debug.Log("Frightened Intruder");
        //    intruder.Frighten(poltergeist, transform.position);
        //    SetAnimation();
        //}
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = gizmoColor;
        Gizmos.DrawSphere(transform.position, influenceRadius);
    }
}
