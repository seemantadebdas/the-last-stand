using UnityEngine;

public abstract class Ghost : MonoBehaviour
{
    [field: SerializeField] public GhostCursor GhostCursorPrefab { get; private set; }
    [field: SerializeField] public int FearCost { get; private set; }
    [field: SerializeField] public int FearDealt { get; private set; }
    public abstract bool CanPerformAction(RaycastHit hitInfo);
    public abstract void PerformAction(RaycastHit hitInfo);
    public abstract void InitializeGhost();
    public abstract void DisableGhost();
    protected abstract void Haunt();
}
